package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;

public class PrimaryKey {
    private String tableName;
    private String columnName;
    private String name;
    private String sequenceNumber;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "PrimaryKey{" +
                "tableName='" + tableName + '\'' +
                ", columnName='" + columnName + '\'' +
                ", name='" + name + '\'' +
                ", sequenceNumber='" + sequenceNumber + '\'' +
                '}';
    }
}
