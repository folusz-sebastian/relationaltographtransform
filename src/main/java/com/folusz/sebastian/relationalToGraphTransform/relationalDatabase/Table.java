package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;

import java.util.List;

public class Table {
    private String name;
    private List<Column> columns;
    private List<Row> rows;
    private boolean isJoinTable;

    public Table() {
    }

    public Table(String name, List<Column> columns, List<Row> rows, boolean isJoinTable) {
        this.name = name;
        this.columns = columns;
        this.rows = rows;
        this.isJoinTable = isJoinTable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public boolean isJoinTable() {
        return isJoinTable;
    }

    public void setJoinTable(boolean joinTable) {
        isJoinTable = joinTable;
    }

    @Override
    public String toString() {
        return "Table{" +
                "name='" + name + '\'' +
                ", columns=" + columns +
                ", rows=" + rows +
                ", isJoinTable=" + isJoinTable +
                '}';
    }
}
