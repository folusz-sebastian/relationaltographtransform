package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;

public class Cell {
    private Column column;
    private String value;

    public Cell() {
    }

    public Cell(Column column, String value) {
        this.column = column;
        this.value = value;
    }

    public Column getColumn() {
        return column;
    }

    public void setColumn(Column column) {
        this.column = column;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String simpleToString() {
        return column.getName() + ":" + value;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "column=" + column +
                ", value='" + value + '\'' +
                '}';
    }
}
