package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;


public class SQLType {
    private int dataType;
    private String typeName;

    public SQLType(int dataType, String typeName) {
        this.dataType = dataType;
        this.typeName = typeName;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "SQLType{" +
                "dataType=" + dataType +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
