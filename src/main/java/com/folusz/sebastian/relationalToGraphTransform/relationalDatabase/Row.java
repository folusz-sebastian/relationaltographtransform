package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Row {
    private List<Cell> cells;
    private List<Cell> primaryKeyCells;
    private List<Cell> foreignKeyCells;

    public Row() {
        this.cells = new ArrayList<>();
        this.primaryKeyCells = new ArrayList<>();
        this.foreignKeyCells = new ArrayList<>();
    }

    public void add(Cell element) {
        cells.add(element);
        if (element.getColumn().isPrimaryKey()) {
            primaryKeyCells.add(element);
        }
        if (element.getColumn().isForeignKey()) {
            foreignKeyCells.add(element);
        }
    }

    public void extractAndSavePrimaryKeyCells() {
        this.primaryKeyCells = this.cells.stream()
                .filter(cell -> cell.getColumn().isPrimaryKey())
                .collect(Collectors.toList());
    }

    public void extractAndSaveForeignKeyCells() {
        this.foreignKeyCells = this.cells.stream()
                .filter(cell -> cell.getColumn().isForeignKey())
                .collect(Collectors.toList());
    }

    public List<Cell> getCells() {
        return cells;
    }

    public List<Cell> getPrimaryKeyCells() {
        return primaryKeyCells;
    }

    public List<Cell> getForeignKeyCells() {
        return foreignKeyCells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    @Override
    public String toString() {
        return "Row [" +
                cells.stream().map(Cell::simpleToString).collect(Collectors.joining(", ", "{", "}")) +
                "]";
    }
}
