package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;

public class Column {
    private String name;
    private SQLType dataType;
    private String size;
    private int decimalDigits;
    private int numPrecRadix;
    private String charOctetLength;
    private int ordinalPosition;
    private boolean primaryKey;
    private PrimaryKey primaryKeyData;
    private boolean foreignKey;
    private ForeignKey foreignKeyData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SQLType getDataType() {
        return dataType;
    }

    public void setDataType(SQLType dataType) {
        this.dataType = dataType;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getDecimalDigits() {
        return decimalDigits;
    }

    public void setDecimalDigits(int decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

    public int getNumPrecRadix() {
        return numPrecRadix;
    }

    public void setNumPrecRadix(int numPrecRadix) {
        this.numPrecRadix = numPrecRadix;
    }

    public String getCharOctetLength() {
        return charOctetLength;
    }

    public void setCharOctetLength(String charOctetLength) {
        this.charOctetLength = charOctetLength;
    }

    public int getOrdinalPosition() {
        return ordinalPosition;
    }

    public void setOrdinalPosition(int ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public PrimaryKey getPrimaryKeyData() {
        return primaryKeyData;
    }

    public void setPrimaryKeyData(PrimaryKey primaryKeyData) {
        this.primaryKeyData = primaryKeyData;
    }

    public boolean isForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }

    public ForeignKey getForeignKeyData() {
        return foreignKeyData;
    }

    public void setForeignKeyData(ForeignKey foreignKeyData) {
        this.foreignKeyData = foreignKeyData;
    }

    @Override
    public String toString() {
        return "Column{" +
                "name='" + name + '\'' +
                ", dataType=" + dataType +
                ", size='" + size + '\'' +
                ", decimalDigits=" + decimalDigits +
                ", numPrecRadix=" + numPrecRadix +
                ", charOctetLength='" + charOctetLength + '\'' +
                ", ordinalPosition=" + ordinalPosition +
                ", primaryKey=" + primaryKey +
                ", primaryKeyData=" + primaryKeyData +
                ", relationship=" + foreignKey +
                ", relationshipData=" + foreignKeyData +
                '}';
    }
}
