package com.folusz.sebastian.relationalToGraphTransform.relationalDatabase;

public class TableWithHistory extends Table {
    private long id;
    private boolean isHistory;
    private boolean isTemporal;
    private long historyTableId;

    public TableWithHistory(Table table) {
        super(table.getName(), table.getColumns(), table.getRows(), table.isJoinTable());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isHistory() {
        return isHistory;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public boolean isTemporal() {
        return isTemporal;
    }

    public void setTemporal(boolean temporal) {
        isTemporal = temporal;
    }

    public long getHistoryTableId() {
        return historyTableId;
    }

    public void setHistoryTableId(long historyTableId) {
        this.historyTableId = historyTableId;
    }

    @Override
    public String toString() {
        return "Table{" +
                "name='" + getName() + '\'' +
                ", columns=" + getColumns() +
                ", rows=" + getRows() +
                ", isJoinTable=" + isJoinTable() +
                ", id=" + id +
                ", isHistory=" + isHistory +
                ", isTemporal=" + isTemporal +
                ", historyTableId=" + historyTableId +
                '}';
    }
}
