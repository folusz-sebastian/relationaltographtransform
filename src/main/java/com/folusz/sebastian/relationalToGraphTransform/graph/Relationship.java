package com.folusz.sebastian.relationalToGraphTransform.graph;

import java.util.List;

public class Relationship {
    String type;
    List<Property> properties;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }
}
