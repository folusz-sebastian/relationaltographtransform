package com.folusz.sebastian.relationalToGraphTransform;

import com.folusz.sebastian.relationalToGraphTransform.config.Config;
import com.folusz.sebastian.relationalToGraphTransform.relationalDatabase.TableWithHistory;
import com.folusz.sebastian.relationalToGraphTransform.services.GraphDbService;
import com.folusz.sebastian.relationalToGraphTransform.services.RelationalDbService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.util.Set;

public class RelationalToGraphTransformApplication {

    public static void main(String[] args) {
        Set<TableWithHistory> tables;
        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class)) {
            tables = ctx.getBean(RelationalDbService.class).retrieveDbDataWithMetaData();
            ctx.getBean(GraphDbService.class).createGraphBasedOnRelationalDB(tables);
        }
    }

}
