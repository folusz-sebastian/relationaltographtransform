package com.folusz.sebastian.relationalToGraphTransform.services;

import com.folusz.sebastian.relationalToGraphTransform.relationalDatabase.*;
import com.folusz.sebastian.relationalToGraphTransform.graph.Node;
import com.folusz.sebastian.relationalToGraphTransform.graph.Property;
import com.folusz.sebastian.relationalToGraphTransform.graph.Relationship;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class GraphDbService {
    private final Driver dataSource;
    private final CypherService cypherService;
    private final DatabaseObjectsTransformService dbObjectsConverter;

    @Autowired
    public GraphDbService(CypherService cypherService, DatabaseObjectsTransformService dbObjectsConverter) {
        this.cypherService = cypherService;
        this.dbObjectsConverter = dbObjectsConverter;
        this.dataSource = GraphDatabase.driver(
                "bolt://localhost:7687", AuthTokens.basic("neo4j", "qazw"));
    }

    public void createGraphBasedOnRelationalDB(Set<TableWithHistory> tables) {
        Session session = dataSource.session();

        replaceHistoryTableDataWithCorrespondingTemporalTableData(tables);
        createNodes(tables, session);
        createRelationships(tables, session);

        session.close();
        dataSource.close();
    }

    private void replaceHistoryTableDataWithCorrespondingTemporalTableData(Set<TableWithHistory> tables) {
        for (TableWithHistory table : tables) {
            if (table.isHistory()) {
                final Optional<TableWithHistory> correspondingTemporalTable = findTable(tables, table.getId());
                if (correspondingTemporalTable.isPresent()) {
                    replaceHistoryTableColumnsWithTemporalTableColumns(correspondingTemporalTable.get(), table);
                    table.setJoinTable(correspondingTemporalTable.get().isJoinTable());
                    table.setName(correspondingTemporalTable.get().getName());
                }
            }
        }
    }

    private Optional<TableWithHistory> findTable(Set<TableWithHistory> tables, long tableId) {
        return tables.stream().filter(foundedTable -> foundedTable.getHistoryTableId() == tableId).findFirst();
    }

    private void replaceHistoryTableColumnsWithTemporalTableColumns(TableWithHistory correspondingTemporalTable, TableWithHistory historyTable) {
        historyTable.setColumns(correspondingTemporalTable.getColumns());
        for (Row row : historyTable.getRows()) {
            for (int i = 0; i < row.getCells().size(); i++) {
                row.getCells().get(i).setColumn(historyTable.getColumns().get(i));
            }
            row.extractAndSavePrimaryKeyCells();
            row.extractAndSaveForeignKeyCells();
        }
    }

    private void createNodes(Set<TableWithHistory> tables, Session session) {
        for (TableWithHistory table : tables) {
            if (!table.isJoinTable()) {
                for (Row row : table.getRows()) {
                    final Node node = dbObjectsConverter.tableToNode(table.getName(), row);
                    session.run(cypherService.createNode(node));
                }
            }
        }
    }

    private void createRelationships(Set<TableWithHistory> tables, Session session) {
        for (TableWithHistory table : tables) {

            for (Row row : table.getRows()) {
                final List<Node> relatedNodes = dbObjectsConverter.foreignKeysToNodes(row.getForeignKeyCells());
                if (table.isJoinTable() && relatedNodes.size() == 2) {
                    final List<Property> properties = dbObjectsConverter.cellsToProperties(row.getCells());
                    final Property currentProperty = createCurrentNodeProperty(table.isHistory());
                    properties.add(currentProperty);
                    final Relationship relationship = new Relationship();
                    relationship.setType(table.getName());
                    relationship.setProperties(properties);
                    final String query = cypherService.createRelationshipWithUnknownNodes(relatedNodes.get(0), relatedNodes.get(1), relationship);
                    session.run(query);
                } else {
                    for (Node relatedNode : relatedNodes) {
                        final Relationship relationship = new Relationship();
                        final List<Property> nodeProperties = dbObjectsConverter.cellsToProperties(row.getPrimaryKeyCells());
                        final Node node1 = new Node(table.getName(), nodeProperties);
                        final Node node2 = relatedNode;
                        relationship.setType(node1.getType() + "_" + node2.getType());
                        final Property currentProperty = createCurrentNodeProperty(table.isHistory());
                        relationship.setProperties(Collections.singletonList(currentProperty));
                        final String query = cypherService.createRelationshipWithUnknownNodes(node1, node2, relationship);
                        session.run(query);
                    }
                }
            }
        }
    }

    private Property createCurrentNodeProperty(boolean tableIsHistory) {
        final Property currentProperty = new Property();
        currentProperty.setKey("current");
        currentProperty.setValue(Boolean.toString(!tableIsHistory));
        return currentProperty;
    }
}
