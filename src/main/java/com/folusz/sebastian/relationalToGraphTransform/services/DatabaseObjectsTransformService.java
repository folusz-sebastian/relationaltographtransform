package com.folusz.sebastian.relationalToGraphTransform.services;

import com.folusz.sebastian.relationalToGraphTransform.relationalDatabase.Cell;
import com.folusz.sebastian.relationalToGraphTransform.relationalDatabase.Row;
import com.folusz.sebastian.relationalToGraphTransform.graph.Node;
import com.folusz.sebastian.relationalToGraphTransform.graph.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class DatabaseObjectsTransformService {
    private final DataTypesTransformationService typesConverter;

    @Autowired
    public DatabaseObjectsTransformService(DataTypesTransformationService typesConverter) {
        this.typesConverter = typesConverter;
    }

    public Node tableToNode(String tableName, Row tableRow) {
        final Node node = new Node();
        node.setType(tableName);
        final List<Property> properties = cellsToProperties(tableRow.getCells());
        node.setProperties(properties);
        return node;
    }

    public List<Property> cellsToProperties(List<Cell> cells) {
        return cells.stream().map(this::cellToProperty).collect(Collectors.toList());
    }

    public Property cellToProperty(Cell cell) {
        final Property property = new Property();
        property.setKey(cell.getColumn().getName());
        final int dataType = cell.getColumn().getDataType().getDataType();
        String propertyValue = typesConverter.convertRelationalToGraphDataType(dataType, cell.getValue());
        property.setValue(propertyValue);
        return property;
    }

    public List<Node> foreignKeysToNodes(final List<Cell> foreignKeys) {
        final Map<String, List<Cell>> foreignKeysGroupedByTableName = foreignKeys.stream()
                .collect(groupingBy(cell -> cell.getColumn().getForeignKeyData().getPkTableName()));
        return foreignKeysGroupedByTableName
                .entrySet().stream()
                .map(entry -> {
                    final String tableName = entry.getKey();
                    final List<Property> properties = cellsToProperties(entry.getValue());
                    return new Node(tableName, properties);
                })
                .collect(Collectors.toList());
    }
}
