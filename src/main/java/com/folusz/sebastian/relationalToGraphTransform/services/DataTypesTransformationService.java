package com.folusz.sebastian.relationalToGraphTransform.services;

import org.springframework.stereotype.Service;
import java.sql.Types;

@Service
public class DataTypesTransformationService {
    public String convertRelationalToGraphDataType(int dataType, String value) {
        switch(dataType) {
            case Types.BIT :
            case Types.BOOLEAN:
                return stringToBoolean(value);
            case Types.BIGINT:
            case Types.DECIMAL:
            case Types.DOUBLE:
            case Types.FLOAT:
            case Types.INTEGER:
            case Types.NUMERIC:
            case Types.REAL:
            case Types.TINYINT:
            case Types.SMALLINT:
                return stringToCypherNumber(value);
            case Types.DATE: return stringToCypherDate(value);
            case Types.TIME:
            case Types.TIME_WITH_TIMEZONE:
                return stringToCypherTime(value);
            case Types.TIMESTAMP:
            case Types.TIMESTAMP_WITH_TIMEZONE:
                return stringToCypherDateTime(value);
            default:
                return stringToCypherText(value);
        }
    }

    private String stringToCypherDate(String value) {
        return "\"" + value + "\"";
    }

    private String stringToCypherTime(String value) {
        return value;
    }

    private String stringToCypherDateTime(String value) {
        final String[] dateAndTime = value.split(" ");
        return "datetime(\"" + dateAndTime[0] + "T" + dateAndTime[1] + "\")";
    }

    private String stringToCypherNumber(String value) {
        return value;
    }

    private String stringToCypherText(String value) {
        return "\"" + value + "\"";
    }

    private String stringToBoolean(String value) {
        return Boolean.valueOf(value).toString();
    }
}
