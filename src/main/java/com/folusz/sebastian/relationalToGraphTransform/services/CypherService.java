package com.folusz.sebastian.relationalToGraphTransform.services;

import com.folusz.sebastian.relationalToGraphTransform.graph.Node;
import com.folusz.sebastian.relationalToGraphTransform.graph.Property;
import com.folusz.sebastian.relationalToGraphTransform.graph.Relationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CypherService {

    @Autowired
    public CypherService() {
    }

    public String createNode(Node node) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CREATE (:");
        stringBuilder.append(node.getType());
        stringBuilder.append(" ");
        final String properties = createProperties(node.getProperties());
        stringBuilder.append(properties);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    public String findNode(Node node, String nodeVariable) {
        return "MATCH (" + nodeVariable + ":" + node.getType() + " " + createProperties(node.getProperties()) + ")";
    }

    public String createRelationShip(Relationship relationship, String startNodeVariable, String endNodeVariable) {
        return "CREATE (" + startNodeVariable + ")" +
                "-[:" + relationship.getType() + " " + createProperties(relationship.getProperties()) + "]->" +
                "(" + endNodeVariable + ")";
    }

    public String createRelationshipWithUnknownNodes(Node node1, Node node2, Relationship relationship) {
        final String findNode1 = findNode(node1, "node1");
        final String findNode2 = findNode(node2, "node2");
        final String createRelationship = createRelationShip(relationship, "node1", "node2");
        return findNode1 + "\n" + findNode2 + "\n" + createRelationship;
    }

    public String createProperties(List<Property> properties) {
        return properties.stream()
                .map(property -> property.getKey() + ":" + property.getValue())
                .collect(Collectors.joining(", ", "{", "}"));
    }
}
