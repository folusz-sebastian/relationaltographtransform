package com.folusz.sebastian.relationalToGraphTransform.services;

import com.folusz.sebastian.relationalToGraphTransform.relationalDatabase.*;
import com.folusz.sebastian.relationalToGraphTransform.relationalDatabase.SQLType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class RelationalDbService {
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private static final String DB_CATALOG = "MusicAlbumsMarket";
    private static final String DB_SCHEMA_PATTERN = "dbo";
    private static final String DB_TABLE_NAME_PATTERN = null;
    private static final String[] DB_TYPES = new String[]{"TABLE"};

    private final DataSource dataSource;

    @Autowired
    public RelationalDbService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Set<TableWithHistory> retrieveDbDataWithMetaData() {
        Set<TableWithHistory> tablesWithHistory = Collections.emptySet();
        try (Connection connection = dataSource.getConnection()) {
            final DatabaseMetaData metaData = connection.getMetaData();
            Set<Table> tables = retrieveTables(metaData);

            for (Table table : tables) {
                final List<PrimaryKey> primaryKeys = retrievePrimaryKeys(metaData, table.getName());
                final List<ForeignKey> foreignKeys = retrieveForeignKeys(metaData, table.getName());
                final List<Column> columns = retrieveColumns(metaData, table.getName(), primaryKeys, foreignKeys);
                table.setColumns(columns);
                table.setRows(retrieveRows(connection, table));
                table.setJoinTable(isJoinTable(table));
            }

            tablesWithHistory = tables.stream()
                    .map(table -> retrieveAdditionalTableMetadata(connection, table))
                    .collect(Collectors.toSet());

            for (TableWithHistory table : tablesWithHistory) {
                LOGGER.info(table.toString());
                LOGGER.info(table.getName() + " columns: ");
                table.getColumns().forEach(column -> LOGGER.info(column.toString()));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tablesWithHistory;
    }

    private Set<Table> retrieveTables(DatabaseMetaData metaData) {
        final Set<Table> tables = new HashSet<>();
        try (ResultSet resultSet = metaData.getTables(DB_CATALOG, DB_SCHEMA_PATTERN, DB_TABLE_NAME_PATTERN, DB_TYPES)) {
            while (resultSet.next()) {
                final Table table = new Table();
                table.setName(resultSet.getString("TABLE_NAME"));
                tables.add(table);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tables;
    }

    private ArrayList<PrimaryKey> retrievePrimaryKeys(DatabaseMetaData metaData, String tableName) {
        ArrayList<PrimaryKey> primaryKeys = new ArrayList<>();
        try (ResultSet resultSet = metaData.getPrimaryKeys(DB_CATALOG, DB_SCHEMA_PATTERN, tableName)) {
            while (resultSet.next()) {
                final PrimaryKey primaryKey = new PrimaryKey();
                primaryKey.setTableName(resultSet.getString("TABLE_NAME"));
                primaryKey.setColumnName(resultSet.getString("COLUMN_NAME"));
                primaryKey.setName(resultSet.getString("PK_NAME"));
                primaryKey.setSequenceNumber(resultSet.getString("KEY_SEQ"));
                primaryKeys.add(primaryKey);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return primaryKeys;
    }

    private List<ForeignKey> retrieveForeignKeys(DatabaseMetaData metaData, String tableName) {
        List<ForeignKey> foreignKeys = new ArrayList<>();
        try (ResultSet foreignKeyResultSet = metaData.getImportedKeys(DB_CATALOG, DB_SCHEMA_PATTERN, tableName)) {
            while (foreignKeyResultSet.next()) {
                final ForeignKey foreignKey = new ForeignKey();
                foreignKey.setFkTableName(foreignKeyResultSet.getString("FKTABLE_NAME"));
                foreignKey.setFkColumnName(foreignKeyResultSet.getString("FKCOLUMN_NAME"));
                foreignKey.setFkName(foreignKeyResultSet.getString("FK_NAME"));
                foreignKey.setPkTableName(foreignKeyResultSet.getString("PKTABLE_NAME"));
                foreignKey.setPkColumnName(foreignKeyResultSet.getString("PKCOLUMN_NAME"));
                foreignKey.setPkName(foreignKeyResultSet.getString("PK_NAME"));
                foreignKey.setSequenceNumber(foreignKeyResultSet.getString("KEY_SEQ"));
                foreignKeys.add(foreignKey);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return foreignKeys;
    }

    private List<Column> retrieveColumns(DatabaseMetaData metaData, String tableName, List<PrimaryKey> primaryKeys,
                                         List<ForeignKey> foreignKeys) {
        List<Column> columns = new ArrayList<>();
        try (ResultSet columnResultSet = metaData.getColumns(DB_CATALOG, DB_SCHEMA_PATTERN, tableName, null)) {

            while (columnResultSet.next()) {
                final Column column = new Column();
                column.setName(columnResultSet.getString("COLUMN_NAME"));
                final SQLType sqlType = new SQLType(
                        columnResultSet.getInt("DATA_TYPE"),
                        columnResultSet.getString("TYPE_NAME")
                );
                column.setDataType(sqlType);
                column.setSize(columnResultSet.getString("COLUMN_SIZE"));
                column.setDecimalDigits(columnResultSet.getInt("DECIMAL_DIGITS"));
                column.setNumPrecRadix(columnResultSet.getInt("NUM_PREC_RADIX"));
                column.setCharOctetLength(columnResultSet.getString("CHAR_OCTET_LENGTH"));
                column.setOrdinalPosition(columnResultSet.getInt("ORDINAL_POSITION"));

                final Optional<PrimaryKey> primaryKey = findPrimaryKey(tableName, column, primaryKeys);
                if (primaryKey.isPresent()) {
                    column.setPrimaryKey(true);
                    column.setPrimaryKeyData(primaryKey.get());
                } else {
                    column.setPrimaryKey(false);
                    column.setPrimaryKeyData(null);
                }

                final Optional<ForeignKey> foreignKey = findForeignKey(tableName, column, foreignKeys);
                if (foreignKey.isPresent()) {
                    column.setForeignKey(true);
                    column.setForeignKeyData(foreignKey.get());
                } else {
                    column.setForeignKey(false);
                    column.setForeignKeyData(null);
                }

                columns.add(column);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return columns;
    }

    private Optional<PrimaryKey> findPrimaryKey(String tableName, Column column, List<PrimaryKey> primaryKeys) {
        return primaryKeys.stream()
                .filter(primaryKey ->
                        primaryKey.getTableName().equals(tableName) &&
                                primaryKey.getColumnName().equals(column.getName()))
                .findFirst();
    }

    private Optional<ForeignKey> findForeignKey(String tableName, Column column, List<ForeignKey> foreignKeys) {
        return foreignKeys.stream()
                .filter(foreignKey ->
                        foreignKey.getFkTableName().equals(tableName) &&
                                foreignKey.getFkColumnName().equals(column.getName()))
                .findFirst();
    }

    private ArrayList<Row> retrieveRows(Connection connection, Table table) {
        ArrayList<Row> rows = new ArrayList<>();
        try (final Statement stmt = connection.createStatement()) {
            String selectSQL = "SELECT * FROM [" + table.getName() + "]";

            try (ResultSet resultSet = stmt.executeQuery(selectSQL)) {
                while (resultSet.next()) {
                    final Row row = new Row();
                    for (Column column : table.getColumns()) {
                        final Cell cell = new Cell(column, resultSet.getString(column.getName()));
                        row.add(cell);
                    }
                    rows.add(row);
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rows;
    }

    private boolean isJoinTable(Table table) {
        final long numberOfPKButNotFKKeys = table.getColumns().stream()
                .filter(column -> !column.isForeignKey() && column.isPrimaryKey()).count();
        final long numberOfFK = table.getColumns().stream()
                .filter(Column::isForeignKey).count();
        return numberOfFK > 0 && numberOfPKButNotFKKeys == 0;
    }

    private TableWithHistory retrieveAdditionalTableMetadata(Connection connection, Table table) {
        TableWithHistory tableWithHistory = new TableWithHistory(table);
        try (final Statement stmt = connection.createStatement()) {
            String selectSQL = "select object_id, temporal_type, history_table_id " +
                    "from sys.tables where name = '" + table.getName() + "'";

            try (ResultSet resultSet = stmt.executeQuery(selectSQL)) {
                while (resultSet.next()) {

                    tableWithHistory.setId(resultSet.getLong(1));
                    final int temporalType = resultSet.getInt(2);
                    tableWithHistory.setHistory(temporalType == 1);
                    tableWithHistory.setTemporal(temporalType == 2);
                    final long historyTableId = resultSet.getLong(3);
                    if (!resultSet.wasNull()) tableWithHistory.setHistoryTableId(historyTableId);
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tableWithHistory;
    }
}
