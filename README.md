# Relational database to graph database transformation

##  Opis projektu
Projekt jest implementacją konsolowej aplikacji realizującej transformację relacyjnej bitemporalnej bazy danych do grafowej bitemporalnej bazy danych

W projekcie została wykorzystana gotowa bitemporalna relacyjna baza danych z repozytorium:  
[https://bitbucket.org/folusz-sebastian/temporal-database-for-music-shop/src/master/](https://bitbucket.org/folusz-sebastian/temporal-database-for-music-shop/src/master/)